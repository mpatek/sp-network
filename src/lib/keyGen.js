const randomBytes = require('randombytes');

/* Randomly generates a key */
const keyGen = () => {
  return randomBytes(2);
};

module.exports = keyGen;