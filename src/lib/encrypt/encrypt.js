const fs = require('fs');
const spNetwork = require('./spNetwork');

const encrypt = (key, filename) => {
  if (!fs.existsSync(filename)) {
    throw new Error(`Path ${filename} does not exist`);
  } else if (!Buffer.isBuffer(key)) {
    throw new Error('key must be of type Buffer');
  }

  const plainText = fs.readFileSync(filename);
  let cipherText = Buffer.alloc(plainText.length);

  // for each byte
  for (let i = 0; i < plainText.length; i++) {
    // xor with first part of key
    const k0 = Buffer.from([plainText[i] ^ key[0]]);

    // pump into substitution-permutation network
    const buffer = spNetwork(k0);

    // xor with second part of key
    const k1 = Buffer.from([buffer.readUInt8() ^ key[1]]);
    
    cipherText[i] = k1.readUInt8();
  }

  fs.writeFileSync(filename, cipherText);
};

module.exports = encrypt;
