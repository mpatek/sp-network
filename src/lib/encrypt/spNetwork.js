const pbox = require('./pbox');
const sbox = require('./sbox');

const spNetwork = (chunk) => {
  if (!chunk) {
    throw new Error('chunk not passed');
  } else if (chunk.length !== 1) {
    throw new Error('chunk size must be 1 byte');
  } else if (!Buffer.isBuffer(chunk)) {
    throw new Error('chunk is not a buffer');
  }

  const sbox_output = sbox(chunk);
  const pbox_output = pbox(sbox_output);
  return pbox_output;
};

module.exports = spNetwork;