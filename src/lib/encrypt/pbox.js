/* Permutation Box */
const pbox = (chunk) => {
  // bit manipulation
  const buf0 = Buffer.from([(chunk.readUInt8() & 0x01) << 4]);
  const buf1 = Buffer.from([(chunk.readUInt8() & 0x02) << 1]);
  const buf2 = Buffer.from([(chunk.readUInt8() & 0x04) >> 2]);
  const buf3 = Buffer.from([(chunk.readUInt8() & 0x08) << 2]);
  const buf4 = Buffer.from([(chunk.readUInt8() & 0x10) << 2]);
  const buf5 = Buffer.from([(chunk.readUInt8() & 0x20) >> 4]);
  const buf6 = Buffer.from([(chunk.readUInt8() & 0x40) << 1]);
  const buf7 = Buffer.from([(chunk.readUInt8() & 0x80) >> 4]);

  // merge everything together
  return Buffer.from([
    buf0.readUInt8()
    ^ buf1.readUInt8()
    ^ buf2.readUInt8()
    ^ buf3.readUInt8()
    ^ buf4.readUInt8()
    ^ buf5.readUInt8()
    ^ buf6.readUInt8()
    ^ buf7.readUInt8()
  ]);
};

module.exports = pbox;