/* Inverted Substitution Box */
const isbox = (chunk) => {
  const upperChunk = upper(chunk).readUInt8();
  const lowerChunk = lower(chunk).readUInt8();
  return Buffer.from([upperChunk ^ lowerChunk]);
};

/* masks lower 4 bits and returns a mapped response */
const upper = (chunk) => {
  const maskedChunk = Buffer.from([chunk.readUInt8() & 0xF0]);
  
  switch (maskedChunk.readUInt8()) {
    case 0: return Buffer.from([0xA0]);
    case 16: return Buffer.from([0x70]);
    case 32: return Buffer.from([0xB0]);
    case 48: return Buffer.from([0x00]);
    case 64: return Buffer.from([0xC0]);
    case 80: return Buffer.from([0xD0]);
    case 96: return Buffer.from([0xF0]);
    case 112: return Buffer.from([0x10]);
    case 128: return Buffer.from([0x40]);
    case 144: return Buffer.from([0x50]);
    case 160: return Buffer.from([0x90]);
    case 176: return Buffer.from([0x80]);
    case 192: return Buffer.from([0x60]);
    case 208: return Buffer.from([0xE0]);
    case 224: return Buffer.from([0x20]);
    case 240: return Buffer.from([0x30]);
  }
};

/* masks upper 4 bits and returns a mapped response */
const lower = (chunk) => {
  const maskedChunk = Buffer.from([chunk.readUInt8() & 0x0F]);

  switch (maskedChunk.readUInt8()) {
    case 0: return Buffer.from([0x0A]);
    case 1: return Buffer.from([0x04]);
    case 2: return Buffer.from([0x01]);
    case 3: return Buffer.from([0x0B]);
    case 4: return Buffer.from([0x07]);
    case 5: return Buffer.from([0x06]);
    case 6: return Buffer.from([0x02]);
    case 7: return Buffer.from([0x0E]);
    case 8: return Buffer.from([0x05]);
    case 9: return Buffer.from([0x00]);
    case 10: return Buffer.from([0x03]);
    case 11: return Buffer.from([0x08]);
    case 12: return Buffer.from([0x0F]);
    case 13: return Buffer.from([0x09]);
    case 14: return Buffer.from([0x0D]);
    case 15: return Buffer.from([0x0C]);
  }
};

module.exports = isbox;