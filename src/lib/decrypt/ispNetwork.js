const ipbox = require('./ipbox');
const isbox = require('./isbox');

const ispNetwork = (chunk) => {
  if (!chunk) {
    throw new Error('chunk not passed');
  } else if (chunk.length !== 1) {
    throw new Error('chunk size must be 1 byte');
  } else if (!Buffer.isBuffer(chunk)) {
    throw new Error('chunk is not a buffer');
  }

  const ipbox_output = ipbox(chunk);
  const isbox_output = isbox(ipbox_output);
  return isbox_output;
};

module.exports = ispNetwork;