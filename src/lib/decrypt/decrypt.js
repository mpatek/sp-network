const fs = require('fs');
const ispNetwork = require('./ispNetwork');

const decrypt = (key, filename) => {
  if (!fs.existsSync(filename)) {
    throw new Error(`Path ${filename} does not exist`);
  } else if (!Buffer.isBuffer(key)) {
    throw new Error('key must be of type Buffer');
  }

  const cipherText = fs.readFileSync(filename);
  let plainText = Buffer.alloc(cipherText.length);

  // for each byte
  for (let i = 0; i < cipherText.length; i++) {
    // xor with first part of key
    const k0 = Buffer.from([cipherText[i] ^ key[1]]);

    // pump into substitution-permutation network
    const buffer = ispNetwork(k0);

    // xor with second part of key
    const k1 = Buffer.from([buffer.readUInt8() ^ key[0]]);
    
    plainText[i] = k1.readUInt8();
  }

  fs.writeFileSync(filename, plainText);
};

module.exports = decrypt;