const decrypt = require('./decrypt/decrypt');
const encrypt = require('./encrypt/encrypt');
const ipbox = require('./decrypt/ipbox');
const isbox = require('./decrypt/isbox');
const ispNetwork = require('./decrypt/ispNetwork');
const keyGen = require('./keyGen');
const pbox = require('./encrypt/pbox');
const sbox = require('./encrypt/sbox');
const spNetwork = require('./encrypt/spNetwork');

module.exports = {
  decrypt,
  encrypt,
  ipbox,
  isbox,
  ispNetwork,
  keyGen,
  pbox,
  sbox,
  spNetwork
}