const { expect } = require('chai');
const { keyGen } = require('../lib');

describe('keyGen testing', () => {
  it('Should generate a buffer', () => {
    const key = keyGen();
    expect(Buffer.isBuffer(key)).to.be.true;
  });

  it('Should generate a random key', () => {
    const key1 = keyGen();
    const key2 = keyGen();

    expect(key1.compare(key2)).to.not.equal(0);
  });
});