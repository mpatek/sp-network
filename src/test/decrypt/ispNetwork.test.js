const { expect } = require('chai');
const { ispNetwork, spNetwork } = require('../../lib');

describe('ispNetwork testing', () => {
  it('Should fail when nothing is passed', () => {
    try {
      ispNetwork();
      expect(false).to.be.true;
    } catch (err) {
      expect(err.message).to.equal('chunk not passed');
    }
  });

  it('Should fail when more than one byte is passed', () => {
    try {
      ispNetwork(Buffer.from('12'));
      expect(false).to.be.true;
    } catch (err) {
      expect(err.message).to.equal('chunk size must be 1 byte');
    }
  });

  it('Should fail when the chunk is not a buffer', () => {
    try {
      ispNetwork('1');
      expect(false).to.be.true;
    } catch (err) {
      expect(err.message).to.equal('chunk is not a buffer');
    }
  });

  it('Should pass when one byte is passed', () => {
    ispNetwork(Buffer.from('1'));
  });

  it('Should return a buffer', () => {
    const buffer = Buffer.from('1');
    expect(Buffer.isBuffer(ispNetwork(buffer))).to.be.true;
  });

  it('Should return a one byte buffer', () => {
    const buffer = Buffer.from('1');
    expect(ispNetwork(buffer).length).to.equal(1);    
  });

  it('Should invert spNetwork', () => {
    for (let i = 0; i < 256; i++) {
      const temp = Buffer.from([i]);
      const sp = spNetwork(temp);
      const isp = ispNetwork(sp);
      expect(isp.compare(temp)).to.equal(0);
    }
  });
});