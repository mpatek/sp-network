const { expect } = require('chai');
const { ipbox, pbox } = require('../../lib');

describe('ipbox testing', () => {
  it('Should pass when one byte is passed', () => {
    ipbox(Buffer.from('1'));
  });

  it('Should return a buffer', () => {
    const buffer = Buffer.from('1');
    expect(Buffer.isBuffer(ipbox(buffer))).to.be.true;
  });

  it('Should return a one byte buffer', () => {
    const buffer = Buffer.from('1');
    expect(ipbox(buffer).length).to.equal(1);    
  });

  it('Should invert pbox', () => {
    for (let i = 0; i < 256; i++) {
      const temp = Buffer.from([i]);
      const perm = pbox(temp);
      const iperm = ipbox(perm);
      expect(iperm.compare(temp)).to.equal(0);
    }
  });
});