const { expect } = require('chai');
const { isbox, sbox } = require('../../lib');

describe('isbox testing', () => {
  it('Should pass when one byte is passed', () => {
    isbox(Buffer.from('1'));
  });

  it('Should return a buffer', () => {
    const buffer = Buffer.from('1');
    expect(Buffer.isBuffer(isbox(buffer))).to.be.true;
  })

  it('Should return a one byte buffer', () => {
    const buffer = Buffer.from('1');
    expect(isbox(buffer).length).to.equal(1);    
  });

  it('Should perform a substitution on a buffer', () => {
    const buf1 = Buffer.from([0xa7]);
    const buf2 = isbox(buf1);
    expect(buf1.compare(buf2)).to.not.equal(0, `${buf2.readUInt8()} should not match ${buf1.readUInt8()}`);
  });

  it('Should create unique values', () => {
    const obj = {};
    let hex = 0x00;

    for (let i = 0; i < 256; i++) {
      const temp = Buffer.from([hex++]);
      const sub = isbox(temp).readUInt8();

      if (obj[sub]) {
        expect(obj[sub]).to.be.undefined;
      }

      obj[sub] = i;
    }
  });

  it('Should invert sbox', () => {
    for (let i = 0; i < 256; i++) {
      const temp = Buffer.from([i]);
      const sub = sbox(temp);
      const isub = isbox(sub);
      expect(isub.compare(temp)).to.equal(0);
    }
  });
});