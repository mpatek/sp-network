const { expect } = require('chai');
const fs = require('fs-extra');
const { decrypt, encrypt } = require('../../lib');
const randomBytes = require('randombytes');

const CIPHER_KEY = Buffer.from([0xA3, 0x2B]);
const TEST_DATA = 'I am a test file';
const TEST_DIR = 'src/test/decrypt/data';
const TEST_FILE = `${TEST_DIR}/test.txt`;

/* Create test data file*/
beforeEach(() => {
  if (!fs.existsSync(TEST_DIR)) {
    fs.mkdirSync(TEST_DIR);
  }
  fs.writeFileSync(TEST_FILE, TEST_DATA);
});

/* Remove test data */
afterEach(() => {
  fs.removeSync(TEST_DIR);
});

describe('decrypt testing', () => {
  it('Should pass when a valid filename is passed', () => {
    decrypt(CIPHER_KEY, TEST_FILE);
  });

  it('Should fail when an invalid filename is passed', () => {
    const invalidFile = 'invalid';

    try {
      decrypt(CIPHER_KEY, invalidFile);
    } catch (err) {
      expect(err.message).to.equal(`Path ${invalidFile} does not exist`);
    }
  });

  it('Should fail when key is not a buffer', () => {
    const invalidKey = 'abcd';

    try {
      decrypt(invalidKey, TEST_FILE);
    } catch (err) {
      expect(err.message).to.equal('key must be of type Buffer');
    }
  });

  it('Should return decrypted data', () => {
    decrypt(CIPHER_KEY, TEST_FILE);
    const cipherData = fs.readFileSync(TEST_FILE);
    expect(cipherData).to.not.equal(TEST_DATA);
  });

  it('Should successfully decrypt encrypted data', () => {
    const LENGTH = 1024; // 1 KB

    for (let i = 0; i < 1000; i++) {
      const testData = randomBytes(LENGTH);
      fs.writeFileSync(TEST_FILE, testData);
  
      encrypt(CIPHER_KEY, TEST_FILE);
      const cipherData = Buffer.from(fs.readFileSync(TEST_FILE));

      decrypt(CIPHER_KEY, TEST_FILE);
      const plainTextData = Buffer.from(fs.readFileSync(TEST_FILE));

      expect(cipherData.compare(plainTextData)).to.not.equal(0, 'Error: encrypted and decrypted data are the same');      
      expect(testData.compare(plainTextData)).to.equal(0, 'Error: not able to decrypt data');
    }
  });
});
