const { expect } = require('chai');
const fs = require('fs-extra');
const { encrypt } = require('../../lib');
const randomBytes = require('randombytes');

const CIPHER_KEY = Buffer.from([0xA3, 0x2B]);
const TEST_DATA = 'I am a test file';
const TEST_DIR = 'src/test/encrypt/data';
const TEST_FILE = `${TEST_DIR}/test.txt`;

/* Create test data file*/
beforeEach(() => {
  if (!fs.existsSync(TEST_DIR)) {
    fs.mkdirSync(TEST_DIR);
  }
  fs.writeFileSync(TEST_FILE, TEST_DATA);
});

/* Remove test data */
afterEach(() => {
  fs.removeSync(TEST_DIR);
});

describe('encrypt testing', () => {
  it('Should pass when a valid filename is passed', () => {
    encrypt(CIPHER_KEY, TEST_FILE);
  });

  it('Should fail when an invalid filename is passed', () => {
    const invalidFile = 'invalid';

    try {
      encrypt(CIPHER_KEY, invalidFile);
    } catch (err) {
      expect(err.message).to.equal(`Path ${invalidFile} does not exist`);
    }
  });

  it('Should fail when key is not a buffer', () => {
    const invalidKey = 'abcd';

    try {
      encrypt(invalidKey, TEST_FILE);
    } catch (err) {
      expect(err.message).to.equal('key must be of type Buffer');
    }
  });

  it('Should return encrypted data', () => {
    encrypt(CIPHER_KEY, TEST_FILE);
    const cipherData = fs.readFileSync(TEST_FILE);
    expect(cipherData).to.not.equal(TEST_DATA);
  });

  it('Should return strongly encrypted data', () => {
    const LENGTH = 4;
    const MIN_DIFF = 4;

    for (let i = 0; i < 10000; i++) {
      let testData = randomBytes(LENGTH);
      fs.writeFileSync(TEST_FILE, testData);
      testData = testData.readUIntBE(0, LENGTH);
  
      encrypt(CIPHER_KEY, TEST_FILE);
      const cipherData = Buffer.from(fs.readFileSync(TEST_FILE)).readUIntBE(0, LENGTH);
      const diff = bitDiffCount(testData, cipherData);

      expect(diff).to.be.at.least(MIN_DIFF, 'Encrypted value is not strong enough');
    }
  });
});

const bitDiffCount = (a, b) => {
  const bitStr = ((a ^ b) >>> 0).toString(2);
  return bitStr.split('1').length - 1;
};