const { expect } = require('chai');
const { pbox } = require('../../lib');

describe('pbox testing', () => {
  it('Should pass when one byte is passed', () => {
    pbox(Buffer.from('1'));
  });

  it('Should return a buffer', () => {
    const buffer = Buffer.from('1');
    expect(Buffer.isBuffer(pbox(buffer))).to.be.true;
  });

  it('Should return a one byte buffer', () => {
    const buffer = Buffer.from('1');
    expect(pbox(buffer).length).to.equal(1);    
  });

  it('Should correctly move the bits', () => {
    const buf1 = Buffer.from([0x7D]);
    const buf2 = Buffer.from([0xC9]);
    const buf3 = Buffer.from([0x45]);
    const buf4 = Buffer.from([0x00]);

    expect(Buffer.from([0xF3]).compare(pbox(buf1))).to.equal(0);
    expect(Buffer.from([0xB8]).compare(pbox(buf2))).to.equal(0);
    expect(Buffer.from([0x91]).compare(pbox(buf3))).to.equal(0);
    expect(Buffer.from([0x00]).compare(pbox(buf4))).to.equal(0);
  });
});