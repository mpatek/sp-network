const { expect } = require('chai');
const { spNetwork } = require('../../lib');

describe('spNetwork testing', () => {
  it('Should fail when nothing is passed', () => {
    try {
      spNetwork();
      expect(false).to.be.true;
    } catch (err) {
      expect(err.message).to.equal('chunk not passed');
    }
  });

  it('Should fail when more than one byte is passed', () => {
    try {
      spNetwork(Buffer.from('12'));
      expect(false).to.be.true;
    } catch (err) {
      expect(err.message).to.equal('chunk size must be 1 byte');
    }
  });

  it('Should fail when the chunk is not a buffer', () => {
    try {
      spNetwork('1');
      expect(false).to.be.true;
    } catch (err) {
      expect(err.message).to.equal('chunk is not a buffer');
    }
  });

  it('Should pass when one byte is passed', () => {
    spNetwork(Buffer.from('1'));
  });

  it('Should return a buffer', () => {
    const buffer = Buffer.from('1');
    expect(Buffer.isBuffer(spNetwork(buffer))).to.be.true;
  });

  it('Should return a one byte buffer', () => {
    const buffer = Buffer.from('1');
    expect(spNetwork(buffer).length).to.equal(1);    
  });

  it('Should create unique values', () => {
    const obj = {};
    let hex = 0x00;

    for (let i = 0; i < 256; i++) {
      const temp = Buffer.from([hex++]);
      const cipher = spNetwork(temp).readUInt8();

      if (obj[cipher]) {
        expect(obj[cipher]).to.be.undefined;
      }

      obj[cipher] = i;
    }
  });
});