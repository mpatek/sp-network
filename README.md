# SP-Network
`encrypt(key, path)`: given a key of type `Buffer`, encrypts the data in `path`, writing the data back to the file

`decrypt(key, path)`: given a key of type `Buffer` (the same used to encrypt the data), decrypts the data and writes it back to file

`keyGen()`: returns a randomly generated 2 byte sized key of type `Buffer`

### Scripts
`npm test`: runs all unit tests